@extends('../layouts/master')

@section('content')
<h1>Calendario</h1>
<div class="table-responsive ">
    <table class="table table-sm block flex-nowrap">
        <thead>
            <tr>
                <th></th>
                <?php for ($i = 0; $i < 11; $i++) : ?>
                    <th><?= date('d-m-Y', strtotime($days_before . ' +' . $i . ' days')); ?></th>
                <?php endfor; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pisos as $piso) : ?>
                <?php foreach ($piso['habitaciones'] as $habitacion) : ?>
                    <tr>
                        <td><?= $habitacion['nombre']; ?></td>
                        <?= implode('', (array)$habitacion['reservas_habitaciones']); ?>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
@endsection