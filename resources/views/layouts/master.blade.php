<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="{{ url('storage/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('storage/assets/bootstrap/css/home.css') }}" rel="stylesheet">

    <title>Calendario</title>
</head>

<body>


    <div class="container">
        @yield('content')
    </div>

    <script src="{{ url('storage/assets/bootstrap/js/bootstrap.min.js') }}"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</body>

</html>