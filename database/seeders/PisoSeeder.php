<?php

namespace Database\Seeders;

use App\Models\Piso;
use Illuminate\Database\Seeder;

class PisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=1; $i < 4; $i++) { 
            (new Piso(['nombre' => 'Piso ' . $i]))->save();
        }
        
        $this->command->info('Tabla pisos inicializada con datos');
    }
}
