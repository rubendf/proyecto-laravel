<?php

namespace Database\Seeders;

use App\Models\Piso;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PisoSeeder::class);
        $this->call(ReservaSeeder::class);
        $this->call(HabitacioneSeeder::class);

    }
}
