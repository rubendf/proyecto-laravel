<?php

namespace Database\Seeders;

use App\Models\Habitacion;
use App\Models\Piso;
use App\Models\Reserva;
use Illuminate\Database\Seeder;

class HabitacioneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $habitaciones = [
            0 => [
                'nombre' => 'Habitacion 1',
            ],
            1 => [
                'nombre' => 'Habitacion 2',
            ],
            2 => [
                'nombre' => 'Habitacion 3',
            ],
            3 => [
                'nombre' => 'Habitacion 4',
            ],
            4 => [
                'nombre' => 'Habitacion 5',
            ],
            5 => [
                'nombre' => 'Habitacion 6',
            ],
        ];


        foreach($habitaciones as $habitacion){
            $habitacion['piso_id'] = Piso::all()->random()->id;
            $habitacion = new Habitacion($habitacion);
            $habitacion->save();
            $habitacion->reservas()->attach([
                Reserva::all()->take(10)->random()->id,
            ]);        
        }

        $this->command->info('Tabla habitaciones inicializada con datos');
    }
}
