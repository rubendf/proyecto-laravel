<?php

namespace App\Http\Controllers;

use App\Models\Piso;
use DateTime;

class CalendarioController extends Controller
{
    public function index()
    {

        $days_before = date('d-m-Y', strtotime('-5 days'));
        $days_after = date('Y-m-d', strtotime('+5 days'));



        $pisos = Piso::all();
        /*$pisos = Habitacion::whereHas('reservas', function ($query){
            $query->orWhere(function($query) {
                $query->where('fecha_desde', '>=', date('d-m-Y', strtotime('-5 days')))
                    ->where('fecha_hasta', '<=', date('Y-m-d', strtotime('+5 days')));
            })
            ->orWhere(function($query) {
                $query->where('fecha_desde', '<', date('d-m-Y', strtotime('-5 days')))
                    ->where('fecha_hasta', '>=', date('d-m-Y', strtotime('-5 days')))
                    ->where('fecha_hasta', '<=', date('Y-m-d', strtotime('+5 days')));
            })
            ->orWhere(function($query) {
                $query->where('fecha_hasta', '>', date('d-m-Y', strtotime('-5 days')))
                    ->where('fecha_desde', '>=', date('d-m-Y', strtotime('-5 days')))
                    ->where('fecha_desde', '<=', date('Y-m-d', strtotime('+5 days')));
            });
        });*/

        foreach ($pisos as $piso) {
            foreach ($piso->habitaciones as $habitacion) {
                if (!empty($habitacion->reservas)) {
                    $rangos = [];
                    $reservas = $habitacion->reservas;
                    foreach ($habitacion->reservas as $reserva) {
                        $equivalencias = ['-5' => 0, '-4' => 1, '-3' => 2, '-2' => 3, '-1' => 4, '0' => 5, '1' => 6, '2' => 7, '3' => 8, '4' => 9, '5' => 10];
                        $fecha_desde = date("d-m-Y", strtotime($reserva->fecha_desde));
                        $fecha_hasta = date("d-m-Y", strtotime($reserva->fecha_hasta));

                        $now = new DateTime("TODAY");
                        $fecha_desde = new DateTime($fecha_desde);
                        $fecha_hasta = new DateTime($fecha_hasta);
                        $diff_fecha_desde = $now->diff($fecha_desde)->format("%r%a");
                        $diff_fecha_hasta = $now->diff($fecha_hasta)->format("%r%a");

                        $rangos[] = [$diff_fecha_desde < -5 || $diff_fecha_desde > 5  ? '' : $equivalencias[$diff_fecha_desde], $diff_fecha_hasta > 5 || $diff_fecha_hasta < -5  ? '' : $equivalencias[$diff_fecha_hasta]];
                    }
                    $resultado = [0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '', 6 => '', 7 => '', 8 => '', 9 => '', 10 => ''];

                    $array_rangos = array_fill_keys(array_column($rangos, 0), 'inicio') + array_fill_keys(array_column($rangos, 1), 'fin');
                    $resultado = array_replace($resultado, $array_rangos);

                    for ($i = 0; $i < 11; $i++) {
                        if ($resultado[$i] == 'inicio') {
                            $contador = 0;
                            $inicio = $i;
                            while ($resultado[$i] != 'fin') {
                                $contador++;
                                $i++;
                                if ($i == 11) {
                                    $resultado[$inicio] = '<td colspan="' . ($contador + 1) . '" style="background-color: green; border: 1px solid black; border-right: none;"></td>';
                                    $resultado = array_diff_key($resultado, array_flip(range($inicio + 1, $i - 1)));
                                    continue 2;
                                }
                            }
                            $resultado[$inicio] = '<td colspan="' . ($contador + 1) . '" style="background-color: green; border: 1px solid black;"></td>';
                            $resultado = array_diff_key($resultado, array_flip(range($inicio + 1, $i)));
                        } elseif ($resultado[$i] == 'fin') {
                            $resultado[$i] = '<td colspan="' . ($i + 1) . '" style="background-color: green; border: 1px solid black; border-left: none;"></td>';
                            if ($i != 0) {
                                $resultado = array_diff_key($resultado, array_flip(range(0, $i - 1)));
                            }
                        } else {
                            $resultado[$i] = '<td></td>';
                        }
                    }
                } else {
                    $resultado = str_repeat('<td></td>', 11);
                }
                if(isset($resultado[''])) unset($resultado['']);
                $habitacion['reservas_habitaciones'] = $resultado;
            }
        }


        return view('Calendario.index', compact('days_before', 'pisos'));
    }
}
