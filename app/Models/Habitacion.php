<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Habitacion extends Model
{
    use HasFactory;
    protected $table = 'habitaciones';
    protected $guarded = [];


    public function reservas(){
        return $this->belongsToMany(Reserva::class);
    }

    public function piso(){
        return $this->belongsTo(Piso::class);
    }
}
