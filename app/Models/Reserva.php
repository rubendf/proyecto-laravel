<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    use HasFactory;
    protected $table = 'reservas';
    protected $guarded = [];

    public function habitaciones(){
        return $this->belongsToMany(Habitacion::class);
    }
}
